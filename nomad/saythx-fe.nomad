job "saythx-fe" {
	datacenters = ["dc1"]
	group "saythx-fe" {
		task "fe" {
			driver = "docker"
			config = {
				image = "eslrain/saythx-fe"
				port_map {
					fe = 80
				}
			}
			resources {
				network {
					mbits = 20
					port "fe" {}
				}
			}

			service {
				name = "saythx-fe"
				port = "fe"
			}
		}
	}

}

