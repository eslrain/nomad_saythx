job "redis" {
	datacenters = ["dc1"]
	type = "service"
	group "redis" {
		task "redis" {
			driver = "docker"
			config {
				image = "redis:5"
				port_map {
					redis = 6379
				}
			}
			resources {
				network {
					mbits = 10
					port "redis" {
						static=6379
					}
				}
			}
			service {
				name = "redis"
				port = "redis"
			}
		}
		task "redis-proxy" {
			driver = "raw_exec"
			config {
				command="/usr/local/bin/consul"
				args=[
					"connect", "proxy",
					"-service", "redis",
					"-log-level", "trace",
					"-service-addr", "${NOMAD_ADDR_redis_redis}",
					"-http-addr=127.0.0.1:8501",
					"-listen", ":${NOMAD_PORT_tcp}",
					"-register",
				]
			}
			env {
				CONSUL_HTTP_SSL=true
				CONSUL_CACERT="/etc/consul.d/cert/consul-agent-ca.pem"
				CONSUL_CLIENT_CERT="/etc/consul.d/cert/dc1-client-consul-0.pem"
				CONSUL_CLIENT_KEY="/etc/consul.d/cert/dc1-client-consul-0-key.pem"
			}
			resources {
				network {
					port "tcp" {}
				}
			}
		}
	}
}

