job "saythx-be" {
	datacenters = ["dc1"]
	group "saythx-be" {
		task "be" {
			driver = "docker"
			config = {
				image = "eslrain/saythx-be"
				port_map {
					be = 8080
				}
			}
			env {
				REDIS_HOST = "${NOMAD_ADDR_be-proxy_tcp}"
			}
			resources {
				network {
					mbits = 10
					port "be" {
						static = "8080"
					}
				}
			}
			service {
				name = "saythx-be"
				port = "be"
			}
		}
		task "be-proxy" {
			driver = "raw_exec"

			config {
				command="/usr/local/bin/consul"
				args = [
					"connect", "proxy",
					"-service", "saythx-be",
					"-http-addr=127.0.0.1:8501",
					"-upstream", "redis:0.0.0.0:${NOMAD_PORT_tcp}",
				]
			}
			env {
				CONSUL_HTTP_SSL=true
				CONSUL_CACERT="/etc/consul.d/cert/consul-agent-ca.pem"
				CONSUL_CLIENT_CERT="/etc/consul.d/cert/dc1-client-consul-0.pem"
				CONSUL_CLIENT_KEY="/etc/consul.d/cert/dc1-client-consul-0-key.pem"
			}
			resources {
				network {
					port "tcp" {}
				}
			}
		}
	}

}

