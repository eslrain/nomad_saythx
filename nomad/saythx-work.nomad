job "saythx-work" {
	datacenters = ["dc1"]
	group "saythx-work" {
		task "saythx-work" {
			driver = "docker"
			config = {
				image = "eslrain/saythx-work"
				port_map {
					work = 80
				}
			}
			env {
				REDIS_HOST="${NOMAD_IP_work-proxy_tcp}"
				REDIS_PORT="${NOMAD_PORT_work-proxy_tcp}"
			}
			resources {
				network {
					mbits = 10
					port "work" {}
				}
			}

			service {
				name = "saythx-work"
				port = "work"
			}
		}
		task "work-proxy" {
			driver = "raw_exec"

			config {
				command="/usr/local/bin/consul"
				args = [
					"connect", "proxy",
					"-service", "saythx-work",
					"-http-addr=127.0.0.1:8501",
					"-upstream", "redis:0.0.0.0:${NOMAD_PORT_tcp}",
				]
			}
			env {
				CONSUL_HTTP_SSL=true
				CONSUL_CACERT="/etc/consul.d/cert/consul-agent-ca.pem"
				CONSUL_CLIENT_CERT="/etc/consul.d/cert/dc1-client-consul-0.pem"
				CONSUL_CLIENT_KEY="/etc/consul.d/cert/dc1-client-consul-0-key.pem"
			}
			resources {
				network {
					port "tcp" {}
				}
			}
		}
	}
}

